Carlos Alvarez hosts a meetup group for Amazon resellers in Miami on Saturday, December 15, 2018. The free meetup aims to teach members how to successfully launch and sustain a business selling items on Amazon.

Address: 14254 SW 8th Street, Miami, FL 33184, USA

Phone: 305-709-5891

Website: [https://wizardsofecom.com](https://wizardsofecom.com)
